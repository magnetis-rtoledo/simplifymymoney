# frozen_string_literal: true

Rails.application.routes.draw do
  resources :account_transfers, only: :create
  resources :accounts, only: :index
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
