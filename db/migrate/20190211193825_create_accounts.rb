# frozen_string_literal: true

class CreateAccounts < ActiveRecord::Migration[5.2]
  def change
    create_table :accounts do |t|
      t.string :email
      t.string :password_digest
      t.float :current_balance, default: 0, null: false

      t.timestamps
    end
    add_index :accounts, :email, unique: true
  end
end
