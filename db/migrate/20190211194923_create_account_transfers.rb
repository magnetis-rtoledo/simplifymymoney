# frozen_string_literal: true

class CreateAccountTransfers < ActiveRecord::Migration[5.2]
  def change
    create_table :account_transfers do |t|
      t.references :account, foreign_key: true
      t.references :account_destination, index: true, foreign_key: { to_table: :accounts }
      t.float :value

      t.timestamps
    end
  end
end
