
## The project its using rails api ~> 5.2.2 and ruby 2.5.1
### The framework for tests its RSpec with Guard to help and run more faster
#### Some gems to considerate
> **guard** will help to run tests every time, command to use
```
bundle exec guard
```
> **simplecov** will generate the the coverage for your code
> **database_cleaner** faster way to erase the database when are testing
> **faker** to get random data for tests
> **rubocop** and **rubocop-rspec** to detect vulnerabilities on code
> **pg** to database
>
**Maybe you have some problem to install pg in MacOS IOS. So with  the command above you can solve this**
```
gem install pg --with-pg-config=/Applications/Postgres.app/Contents/Versions/latest/bin/pg_config
```

#### Run server with
```
rails s
```

### Development, Commit and Deploy instructions
#### Using trello to write the features
> write the task in trello and get the final name of url like ***1-set-rails-application***
> create the ***feature*** based on ***development*** branch
```
git checkout development
git checkout -b features/1-set-rails-application
```
> create and execute the task with tests
```
bundle exec guard
```
> if its ok push repository to gitlab

#### Push to gitlab repository
```
git push origin features/1-set-rails-application
```
> and in gitlab repository make the merge to ***development*** branch

#### Push to master branch
> to merge into master branch all tests must be ok, rubocop must be ok too and 100% of code coverage
> merge in gitlab repository page with ***development*** to ***master***
