# frozen_string_literal: true

class AccountsController < ApplicationController
  before_action :protect_application

  def index
    render json: {
      email: current_account.email,
      current_balance: current_account.current_balance
    }, status: :ok
  end


  protected
    def protect_application
      raise 'need be authorized' unless current_account
    end

    def current_account
      @current_account ||= Account.find_by(id: params[:account_id])
    end

end
