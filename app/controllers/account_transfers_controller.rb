# frozen_string_literal: true

class AccountTransfersController < ApplicationController
  before_action :protect_application

  def create
    source_account.transfer_to(destination_account, params[:amount].to_f)
    response = {
      source_account: {
        email: source_account.email,
        current_balance: source_account.current_balance,
        last_transfer: source_account.last_transfer
      },
      destination_account: {
        email: destination_account.email,
        current_balance: destination_account.current_balance,
        last_transfer: destination_account.last_transfer
      }
    }

    render json: response, status: :created
  end


  protected
    def protect_application
      raise 'need be authorized' unless source_account
      raise 'source and destination account must be different' if destination_account.id == source_account.id
      raise 'check the params on the documentation' if params[:amount].nil?
    end

    def source_account
      @source_account ||= Account.find_by(id: params[:source_account_id])
    end

    def destination_account
      @destination_account ||= Account.find_by(id: params[:destination_account_id])
    end
end
