# frozen_string_literal: true

class Account < ApplicationRecord
  validates :current_balance, numericality: { greater_than_or_equal_to: 0 }
  validates :email, presence: true, uniqueness: true, format: { with: URI::MailTo::EMAIL_REGEXP }
  has_secure_password

  has_many :account_transfers
  has_one :last_transfer, -> { order(created_at: :desc) }, class_name: "AccountTransfer"

  def be_enough_to_transfer?(transfer_value)
    current_balance >= transfer_value
  end

  def transfer_to(account_destination, transfer_value)
    raise 'Must be positive value' if transfer_value <= 0
    raise 'Same account' if id == account_destination.id
    raise 'Dont have enough money' unless be_enough_to_transfer?(transfer_value)
    transfer_value = transfer_value.round(2)
    deposit(account_destination, transfer_value)
    update_attribute(:current_balance, current_balance - transfer_value)
    account_destination.update_attribute(:current_balance, account_destination.current_balance + transfer_value)
    reload
    account_destination.reload
    true
  end

  protected

  def deposit(account_destination, transfer_value)
    account_transfers.build(account_id: id, account_destination_id: account_destination.id, value: transfer_value).save!
  end
end
