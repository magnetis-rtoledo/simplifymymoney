# frozen_string_literal: true

class AccountTransfer < ApplicationRecord
  belongs_to :account
  belongs_to :account_destination, class_name: 'Account', foreign_key: 'account_destination_id'
end
