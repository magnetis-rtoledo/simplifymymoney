# frozen_string_literal: true

FactoryBot.define do
  factory :account, class: Account do
    email { Faker::Internet.email }
    current_balance { 1000 }
    password { 'abcd1234!@#$' }
    password_confirmation { 'abcd1234!@#$' }
  end
end
