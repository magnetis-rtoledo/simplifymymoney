# frozen_string_literal: true

require 'rails_helper'

RSpec.describe AccountsController, type: :controller do
  context 'when have account to make operations' do
    let(:account) { create(:account) }
    let(:magnetis_account) { create(:account, email: 'account@magnetis.com.br') }

    describe '::positive situations' do
      before do
        magnetis_account.transfer_to(account, 250)
      end

      it 'current balance based on the transfers' do
        get :index, params: { account_id: account.id }
        expect(json_body['current_balance']).to eq(1250.0)
      end

      it 'current balance based on the transfers for the account source' do
        get :index, params: { account_id: magnetis_account.id }
        expect(json_body['current_balance']).to eq(750.0)
      end
    end

    describe '::negative situations' do
      it 'the account doesnt exists' do
        expect { get :index }.to raise_error('need be authorized')
      end
    end
  end
end
