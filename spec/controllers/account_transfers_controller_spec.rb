# frozen_string_literal: true

require 'rails_helper'

RSpec.describe AccountTransfersController, type: :controller do
  context 'When make transfers' do
    let(:account) { create(:account) }
    let(:magnetis_account) { create(:account, email: 'account@magnetis.com.br') }

    describe '::positive situations' do
      it 'current balance based on the transfers' do
        post :create, params: { source_account_id: account.id, destination_account_id: magnetis_account.id, amount: 315 }
        expect(json_body['source_account']['current_balance']).to eq(685.0)
        expect(json_body['source_account']['last_transfer']).not_to be nil
        expect(json_body['destination_account']['current_balance']).to eq(1315.0)
        expect(json_body['destination_account']['last_transfer']).to be nil
      end
    end

    describe '::negative situations' do
      it 'the account doesnt exists' do
        expect { post :create }.to raise_error('need be authorized')
      end

      it 'the destination account cant be the same of source account' do
        expect { post :create, params: { source_account_id: account.id, destination_account_id: account.id } }.to raise_error('source and destination account must be different')
      end

      context 'when about amount parameter is treated' do
        it 'fail when dont have amount parameter' do
          expect {
            post :create, params: { source_account_id: account.id, destination_account_id: magnetis_account.id }
          }.to raise_error('check the params on the documentation')
        end

        it 'fail when dont have enough money in source account' do
          expect {
            post :create, params: { source_account_id: account.id, destination_account_id: magnetis_account.id, amount: 2000 }
          }.to raise_error('Dont have enough money')
        end
      end
    end
  end
end
