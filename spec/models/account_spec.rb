# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Account, type: :model do
  context 'With Account Validations' do
    it { should validate_numericality_of(:current_balance).is_greater_than_or_equal_to(0) }
    it { should validate_presence_of(:email) }
    it { should validate_uniqueness_of(:email) }
    it { should_not allow_value('abc').for(:email) }
    it { should have_secure_password }
  end

  context 'With Account Balance operations' do
    describe '::Transfer situations' do
      let(:account) { create(:account) }
      let(:account_destination) { create(:account) }

      it 'have money to transfer' do
        expect(account.be_enough_to_transfer?(500)).to be_truthy
      end

      it 'havent money to transfer' do
        expect(account.be_enough_to_transfer?(2000)).to be_falsey
      end

      context 'positive transfers' do
        it 'can transfer to another account' do
          expect(account.transfer_to(account_destination, 100)).to be_truthy
        end

        it 'check right values on transferencies' do
          account.transfer_to(account_destination, 830)
          expect(account.current_balance).to be_equal(170.0)
          expect(account_destination.current_balance).to be_equal(1830.0)

          last_transfer = account.last_transfer
          expect(last_transfer.account_destination.id).to be_equal(account_destination.id)
          expect(last_transfer.value).to be_equal(830.0)
        end
      end

      it 'cant transfer to another account' do
        expect { account.transfer_to(account_destination, 1001) }.to raise_error('Dont have enough money')
      end

      it 'cant transfer with negative value' do
        expect { account.transfer_to(account, -1) }.to raise_error('Must be positive value')
      end

      it 'cant transfer to same account' do
        expect { account.transfer_to(account, 1) }.to raise_error('Same account')
      end

    end
  end
end
